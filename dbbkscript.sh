#!/bin/bash

# Backup databases: MySQL, PostgreSQL
# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##### SET THIS SECTION ####
	#Global folders
	bks_folder="Backups/"
	daily_folder="Weeksdays/"

	#List of days to save in month folder
	declare -a savepoint_day
	savepoint_day=(1 7 14 21 28)

	#Weekdays number of days to peserve
	weekdays_history_size=7

	#Email settings
	email_to="energy1011@gmail.com"
	subject="DBBkScript"
	flag_send_notification=0
###########################

# Error codes:
E_NOTFOUND=2
E_BACKUP=3
E_NOINSTALLED=4

# Create db_list array
declare -a db_list
# Create config array
declare -A config

#desc Read config file
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function read_cfg_file(){
	echo "[...] Reading dbbkscript.cfg" >&2
	if [ ! -f dbbkscript.cfg ]; then
		echo "*Error $E_NOTFOUND : File dbbkscript.cfg doesn't exist." >&2
		exit $E_NOTFOUND;
	fi
	while read -r line || [[ -n "$line" ]];
	do
		if [[ $line =~ ^'['(.*)']'$ ]]; then
			db_list+=("${BASH_REMATCH[1]}")
			dbname=${BASH_REMATCH[1]}
		fi
		if [[ $line =~ (.*)"="(.*) ]]; then
			config[${dbname}${BASH_REMATCH[1]}]=${BASH_REMATCH[2]}
		fi
	done < dbbkscript.cfg
	echo "-> Databases loaded from cfg: ${db_list[@]}" >&2
}

#desc Check if a db exist
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function check_if_db_exist(){
	local f_return=1
	echo "[...] Checking if database ${db} exist" >&2
	# case driver
	case ${config[${db}driver]} in
		"mysql")
			# MySQL
			db_exist=`mysql -h ${config[${db}host]} -u ${config[${db}user]} -p${config[${db}passwd]} -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '${db}'" | grep ${db} | tr -d '[[:space:]]'`
		;;
		"psql")
			# PostgreSQL
			export PGPASSWORD="${config[${db}passwd]}"
			export POSTGRES_PASSWORD="${config[${db}passwd]}"
			echo "psql -U ${config[${db}user]} -c SELECT datname FROM pg_catalog.pg_database WHERE datname LIKE '%${db}%'" > log.txt
			db_exist=`psql -U ${config[${db}user]} -c "SELECT datname FROM pg_catalog.pg_database WHERE datname LIKE '%${db}%'" -h ${config[${db}host]} | grep ${db} | tr -d '[[:space:]]'`
		;;
		*)
			echo "**Error: Driver not found "${config[${db}driver]} >&2
			echo $f_return
			return
		;;
	esac
	if [ "$db_exist" == "$db" ]; then
		f_return=0
		echo $f_return
		return
	else
		echo $f_return
	fi
}

#desc Search in stack = $1 the needle = $2
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function in_array(){
    declare -a stack=("${!1}")
	local f_return=1
	for e in ${stack[*]}; do
		if [ $2 -eq $e ]; then
			f_return=0
			echo $f_return
			return
		fi
	done
		echo $f_return
}

#desc Backup DB
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function bk_db(){
	local f_return=1
	echo "[...] Backing up "$db" database" >&2
	# check if today number day is in savepoint_day
	if [ $(in_array savepoint_day[*] $(date +%d)) ]; then
		# today is not in savepoint then path is weekdays
		path_to_backup=$bks_folder$db_folder"Weeksdays/"
	else
		# today is in savepoint then path is Month
		path_to_backup=$bks_folder$db_folder"Month$(date +%m)/"
	fi
	# Case driver
	case ${config[${db}driver]} in
		"mysql")
			# MySQL
			mysqldump --force --opt -h ${config[${db}host]} -u ${config[${db}user]} -p${config[${db}passwd]} $db > $path_to_backup`date +%Y-%m-%d`-$db.sql
		;;
		"psql")
			# PostgreSQL
			export PGPASSWORD="${config[${db}passwd]}"
			pg_dump -Fp -h "${config[${db}host]}" -U "${config[${db}user]}" $db > $path_to_backup`date +%Y-%m-%d`-$db.sql
		;;
		*)
			echo "**Error: Driver not found "${config[${db}driver]} >&2
			echo $f_return
			return
		;;
	esac
	if [ $? -eq 0 ]; then
		echo "->Backup $db database Ok." >&2
		f_return=0
		echo $f_return
		return
	else
		echo "*** Error $E_BACKUP : Backing up $db database." >&2
		echo $f_return
		return
	fi
}

#desc Check number of copies based in weekdays_history_size
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function check_weekdays_history_size(){
	if [ ! -n $weekdays_history_size ]; then
	 return
	fi
	echo "[...] Checking number of copies based in weekdays_history_size" >&2
	local number_of_files=`ls $bks_folder$db_folder$daily_folder | wc -l`
	# check if number of backups is grater than weekdays_history_size
	if [ $number_of_files -gt $weekdays_history_size ]; then
		# get the oldest file and rm
		rm -f $bks_folder$db_folder$daily_folder`ls $bks_folder$db_folder$daily_folder -tr | head -n 1`
	fi
}

#desc Send email notification
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function send_notification(){
	# check if flag send notificacion is on or off
	if [ $flag_send_notification -eq 0 ]; then
		# don't send nothing
	 	return;
	fi
	printf "[...] Sending email notification : $message" >&2
	if [ "$flag_errors" != 0 ]; then
		subject="DBBkScript ERROR!"
	fi
	printf "\nMessage: $message \nSubject:$subject \nEmail to: $email_to \n" >&2
	# check if mail command exist
	if ! foobar_loc="$(type -p "${config[${db}driver]}")"; then
		echo "\n*** Error: $E_NOINSTALLED not installed mail command." >&2
	else
		echo -e "$message" | mail -s "$subject" $email_to >&2
	fi
}

#desc Iter each database in db_list
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function iter_over_databases(){
	flag_errors=0
	# Iter over databases
	for db in "${db_list[@]}"; do
		# Check if driver command exist
		if ! foobar_loc="$(type -p "${config[${db}driver]}")"; then
		  # Error error notification
		  message=$message"\n*** Error $E_NOINSTALLED: not installed ${config[${db}driver]} command or check driver name in cfg file."
		  flag_errors=$E_BACKUP
		  continue
		fi
		# Get this db folder name
			db_folder=$(date +%Y)"-"${db}"/"
		# Test if database exist
			if [ $(check_if_db_exist $db) -eq "0" ]; then
				echo "-> Database: "$db" found." >&2
				# Check if exist DB folder <year>-<dbname> exist
				check_or_create_folder $bks_folder$(date +%Y)"-"${db}
				# Check if daily folder exist for this db
				check_or_create_folder $bks_folder$db_folder$daily_folder
				# Get month number
				month_number=$(date +%m)
				# Check if month<month_number> exist
				check_or_create_folder $bks_folder$db_folder"Month"$month_number
				# Backup db
				if [ $(bk_db $db) -eq "1" ]; then
					# Error error notification
					message=$message"\n*** Error $E_BACKUP: Backing Database."
					flag_errors=$E_BACKUP
				else
					# Backup Ok
					message=$message"\n->Backing $db Database Ok....."
					# rm old files based in history size
					check_weekdays_history_size $db
				fi
			else
				#Error notification
				message=$message"\n*** Error $E_NOTFOUND: Database "$db" or driver not found."
				flag_errors=$E_BACKUP
			fi
	done
	send_notification
}

#desc Check if a folder exist if not then mkdir
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
#param String $1 folder name to check or create
function check_or_create_folder(){
	if [[ ! -d $1 ]]; then
		echo "[...] Creating folder: $1 " >&2
		mkdir $1
	fi
}

#desc The main funcion
#author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
function main(){
	echo "=== DBBkScript ===" >&2
	# Check for bk main folder
	check_or_create_folder $bks_folder
	read_cfg_file
	iter_over_databases
	exit 0;
}

# Call main function
main
